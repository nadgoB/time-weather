import React, {Component} from 'react'
import './Home.css'

export default class Home extends Component {
    render(){
        return(
            <div className='wrapper'>
                <h1>Приложение Time & Weather</h1>
                <p>Выбери нужный путь</p>
            </div>
        )
    }
}