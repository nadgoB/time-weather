import React, { Component } from 'react'
import Clock from '../Components/Clock'
import './Home.css'

export default class Time extends Component {
    render() {
        return (
            <div className='wrapper'>
                <div className='box'>
                    <div className='circal'>
                        <Clock />
                    </div>
                </div>
            </div>
        )
    }
}