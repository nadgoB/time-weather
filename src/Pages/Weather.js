import React, { Component } from 'react'
import { Alert } from 'react-bootstrap'
import Form from '../Components/Form'
import './Weather.css'

const API_KEY = "57a90a8fcac701e57c9c008de996f569"
export default class Weather extends Component {

    state = {
        description: undefined,
        temp: undefined,
        wind: undefined,
        preassure:undefined,
        city: undefined,
        country: undefined,
        sunrise: undefined,
        sunset: undefined,
        src: undefined,
        error: undefined
    }

    gettingWeather = async (e) => {
        e.preventDefault();
        let city = e.target.elements.city.value

        if (city) {
            const api_url = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`)
            const data = await api_url.json()

            let sunset = data.sys.sunset;
            let sunrise = data.sys.sunset;
            let date = new Date()
            date.setTime(sunset)
            let sunset_date = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()
            date.setTime(sunrise)
            let sunrise_date = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()

            let src = `https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png`

            this.setState({
                description: data.weather[0].description,
                temp: data.main.temp,
                wind: data.wind.speed,
                preassure: data.main.pressure,
                city: data.name,
                country: data.sys.country,
                sunrise: sunrise_date,
                sunset: sunset_date,
                src: src,
                error: undefined
            });
        } else {
            this.setState({
                description: undefined,
                temp: undefined,
                wind: undefined,
                preassure:undefined,
                city: undefined,
                country: undefined,
                sunrise: undefined,
                sunset: undefined,
                src: undefined,
                error: "Введите название города"
            });
        }


    }

    render() {
        return (
            <div className='wrapper'>
                <div className='box'>
                    <Form weather={this.gettingWeather} />
                    {this.state.city &&
                        <div className='info'>
                            <div className='main'>
                                <p>Current weather in <b>{this.state.city}, {this.state.country}</b> is {this.state.description}</p>
                                <img src = {this.state.src} alt='icon of current weather'/>
                            </div>
                            <div className='support'>
                                <p>Temperature: {Math.round(this.state.temp - 273)} °C </p>
                                <p>Preassure: {this.state.preassure} Па</p>
                                <p>Speed of wind: {this.state.wind} m/s</p>
                                <p>Sunrise: {this.state.sunrise}</p>
                                <p>Sunset: {this.state.sunset}</p>
                            </div>
                        </div>
                    }
                    { this.state.error &&
                        <Alert variant='danger'>{this.state.error}</Alert>
                    }
                </div>
            </div>
        )
    }
}