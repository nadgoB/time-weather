import React, {Component} from 'react'
import { Container, Nav} from 'react-bootstrap'
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom'
import Home from '../Pages/Home';
import Time from "../Pages/Time"
import Weather from "../Pages/Weather"

export default class Menu extends Component {
    constructor(props) {
        super(props)
    }
    

    render(){
        return(
            <>
                <Router>
                    <div className='back'>
                        <Container>
                            <Nav justify variant='tabs'>
                            <Nav.Link as={Link} to="/time"> Time </Nav.Link>
                            <Nav.Link as={Link} to="/"> Home </Nav.Link>
                            <Nav.Link as={Link} to="/weather"> Weather </Nav.Link>
                            </Nav>
                        </Container>
                        <Routes>
                            <Route path="/" element={<Home/>} />
                            <Route path="/time" element={<Time/>} />
                            <Route path="/weather" element={<Weather/>} />
                        </Routes>
                    </div>
                </Router>
            </>
        )
    }
}