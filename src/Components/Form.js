import React from 'react'
import { Button, Input } from 'react-bootstrap'
import '../Pages/Weather.css'

const Form = (props) => (
    <form onSubmit={props.weather}>
        <input type="text" name="city" placeholder="Город" />
        <Button variant="primary" type='submit'>Show weather</Button>
    </form>
)

export default Form